﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.AccessControl;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace MsiMessaging
{
    public class DialerData
    {
        public string CustomerNumber { get; set; }
        public string CustomerSub { get; set; }
        public string AgentID  { get; set; }
        public string EventSkill { get; set; }
        public string  MessageExists { get; set; }
        public string ContactId { get; set; }
        public string  PhoneNumber{ get; set; }

    }
}