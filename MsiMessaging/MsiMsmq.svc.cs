﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Messaging;
    

namespace MsiMessaging
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class MsiMsmq : IMsiMsmq
    {
        [OperationBehavior]
        public string PostMessage(string customerNumber, string customerSub, string contactId, string agentId, string phoneNumber)
        {
            var rmQ = new MessageQueue();
            var mqTran = new MessageQueueTransaction();
            var dialerData = new DialerData();
            try
            {
                //var mq = new MSMQQueue();
                //var mqMessage = new MSMQMessage();
                //var qInfo = new MSMQQueueInfo();
                rmQ.Path = ".\\Private$\\" + agentId;
                mqTran.Begin();
                try
                {
                    dialerData.CustomerNumber = customerNumber;
                    dialerData.CustomerSub = customerSub;
                    dialerData.AgentID = agentId;
                    dialerData.ContactId = contactId;
                    dialerData.PhoneNumber = phoneNumber;
                    var message = new Message
                    {
                        Formatter = new XmlMessageFormatter(new Type[] { typeof(DialerData) }),
                        Body = dialerData,
                        Label = agentId
                    };
                    rmQ.Send(message, mqTran);
                    mqTran.Commit();
                }
                catch (Exception e)
                {
                    mqTran.Abort();
                    return "Failure: " + e.Message;
                }

                return "Success";
            }
            catch (Exception e)
            {
                return "Failure: " + e.Message;
            }
        }

        [OperationBehavior]
        public DialerData ReadMessage(string agentId)
        {
            var mbody = new DialerData();
            MessageQueue messageQueue = new MessageQueue
            {
                Path = ".\\private$\\" + agentId,
                Formatter = new XmlMessageFormatter(new Type[] { typeof(DialerData) })
            };
            if (messageQueue.GetAllMessages().Length > 0)
            {
                MessageQueueTransaction mqTran = new MessageQueueTransaction();
                try
                {
                    mqTran.Begin();
                    var readMessage = messageQueue.Receive(mqTran);
                    if (readMessage != null)
                    {
                        mbody = (DialerData) readMessage.Body;
                        mbody.MessageExists = "Success";
                        mbody.EventSkill = readMessage.Id;
                        mqTran.Commit();
                        return mbody;
                    }
                    else
                    {
                        mbody.MessageExists = "No Message";
                        mqTran.Commit();
                        return mbody;
                    }

                }
                catch (Exception e)
                {
                    mbody.MessageExists = "No Message";
                    return mbody;
                }
                finally
                {
                    mqTran.Dispose();
                }
            }
            else
            {
                mbody.MessageExists = "No Message";
                return mbody;
            }

        }

    }
}
